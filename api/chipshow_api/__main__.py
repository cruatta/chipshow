import uvicorn
from chipshow_api.api import app, config
from chipshow_api.config.env import dev
import logging

if dev:
    log_level = logging.DEBUG
else:
    log_level = logging.INFO

uvicorn.run(app, host=config.host, port=config.port, log_level=log_level)
