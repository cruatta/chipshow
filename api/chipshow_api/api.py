from dataclasses import dataclass
from typing import Optional, List

from starlette.requests import Request
from fastapi.responses import JSONResponse
from chipshow_api.player.exec import UnsupportedOperation
from chipshow_api.track.storage import StorageModel, PaginateModel, TrackIdModel
from chipshow_api.track.fs import FileSystem
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI, HTTPException
from chipshow_api.logger import logger
from chipshow_api.config.version import version
from chipshow_api.config.env import dev
from chipshow_api.config.config import DevConfig, RPIConfig

if dev:
    config = DevConfig()
    origins = [
        "http://localhost:80",
        "http://localhost:3000",
    ]
else:
    config = RPIConfig()
    origins = [
        "http://raspberrypi.ruatta.net:80",
        "http://chipshow.ruatta.net:80"
    ]


app = FastAPI(version=version, title="Chipshow API")
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

file_system = FileSystem(config.chiptunes)
storage = StorageModel()
player = config.create_player()
volume = config.create_volume()


@app.exception_handler(ValueError)
async def unicorn_exception_handler(request: Request, exc: ValueError):
    return JSONResponse(status_code=422)


@app.exception_handler(UnsupportedOperation)
async def unicorn_exception_handler(request: Request, exc: UnsupportedOperation):
    return JSONResponse(status_code=400)


@app.on_event("startup")
async def startup_event():
    logger.info("Config: {}".format(config))
    track_paths = file_system.reload_tracks()
    storage.add_tracks(track_paths)


@dataclass
class TrackPath:
    track_id: str
    path: str


@dataclass
class PlayTrack:
    track_id: str
    busy: bool


@app.get("/api/track/{count}")
async def list_tracks(count: int, index: Optional[int] = 0) -> List[TrackPath]:
    ret = []
    for track_model_id, path in storage.paginate_tracks(PaginateModel(index, count)).items():
        ret.append(TrackPath(track_model_id.id, path))
    return ret


@app.put("/api/player/{track_id}")
async def play_track(track_id: str) -> PlayTrack:
    maybe_track = storage.get_path(TrackIdModel(track_id))
    if maybe_track:
        state = await player.play(file_system.to_full_path(maybe_track))
        if state.busy:
            raise HTTPException(503)
        else:
            return PlayTrack(state.track, state.busy)
    else:
        raise HTTPException(404)


@app.delete("/api/player")
async def stop() -> None:
    return player.stop()


@app.put("/api/volume/increase")
async def increase_volume() -> None:
    await volume.increase_volume()


@app.put("/api/volume/decrease")
async def decrease_volume() -> None:
    await volume.decrease_volume()
