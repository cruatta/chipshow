from chipshow_api.player.exec import DevPlayer, ChipshowPlayer
from dataclasses import dataclass
from chipshow_api.player.exec import AudioPlayer
from chipshow_api.volume.exec import Volume, DevVolume
from typing import List


@dataclass()
class Config:
    player: AudioPlayer
    chiptunes: str
    host: str
    port: int
    mixer: str
    mixer_vol_increase: List[str]
    mixer_vol_decrease: List[str]


class DevConfig(Config):
    def __init__(self):
        self.player = "mpv"
        self.chiptunes = "/home/cruatta/Music/chiptunes/xm"
        self.host = "127.0.0.1"
        self.port = 8000
        self.mixer = "pactl"
        self.mixer_vol_increase = ["set-sink-volume", "1", "+10%"]
        self.mixer_vol_decrease = ["set-sink-volume", "1", "-10%"]

    def create_player(self):
        return DevPlayer(self.player)

    def create_volume(self):
        return DevVolume(self.mixer, self.mixer_vol_increase, self.mixer_vol_decrease)


class RPIConfig(Config):
    def __init__(self):
        self.player = "/opt/chipshow/bin/player-chipshow"
        self.chiptunes = "/opt/chiptunes/xm"
        self.host = "0.0.0.0"
        self.port = 8080
        self.mixer = "amixer"
        self.mixer_vol_increase = ["-M", "sset", "PCM", "Playback", "Volume", "10%+"]
        self.mixer_vol_decrease = ["-M", "sset", "PCM", "Playback", "Volume", "10%-"]

    def create_player(self):
        return ChipshowPlayer(self.player)

    def create_volume(self):
        return Volume(self.mixer, self.mixer_vol_increase, self.mixer_vol_decrease)
