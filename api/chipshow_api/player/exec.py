import asyncio
from chipshow_api.track.fs import PathModel
from asyncio.subprocess import Process
from typing import Optional
from queue import Queue, Full
from dataclasses import dataclass
from fastapi.logger import logger


@dataclass
class PlayerState:
    busy: bool
    track: str


@dataclass
class PlayerProcessState:
    stdout: bytes
    stderr: bytes


class UnsupportedOperation(Exception):
    pass


class AudioPlayer(object):
    def __init__(self, executable: str):
        self.proc: Optional[Process] = None
        self.queue = Queue(1)
        self.executable = executable

    async def play(self, p: PathModel) -> PlayerState:
        try:
            self.queue.put_nowait(p)
            asyncio.create_task(self.worker(self.queue))
            return PlayerState(False, p.filename)
        except Full:
            return PlayerState(True, self.queue.queue[0].filename)

    def stop(self):
        if self.proc and not self.proc.returncode:
            self.proc.kill()
        return

    async def worker(self, queue: Queue) -> PlayerProcessState:
        head: PathModel = queue.queue[0]
        self.proc = await asyncio.create_subprocess_exec(
            self.executable,
            "{}".format(head.path),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        stdout, stderr = await self.proc.communicate()
        queue.get()
        queue.task_done()
        return PlayerProcessState(stdout, stderr)


class DevPlayer(AudioPlayer):
    def __init__(self, player_path):
        super().__init__(player_path)

    async def worker(self, queue: Queue):
        logger.info("executable: {}".format(self.executable))
        logger.info("file: {}".format(queue.queue[0].path))
        state = await super().worker(queue)
        logger.info("stdout: {}".format(state.stdout))
        logger.info("stderr: {}".format(state.stderr))


class ChipshowPlayer(AudioPlayer):
    def __init__(self, chipshow_path):
        super().__init__(chipshow_path)

    async def worker(self, queue: Queue):
        await super().worker(queue)
