from typing import List, Dict, Optional
from hashlib import sha256
import re
from chipshow_api.track.fs import PathModel


class PaginateModel(object):
    def __init__(self, index, count):
        if index < 0 or count < 0:
            raise ValueError("Invalid Model")
        self.index = index
        self.count = count


class TrackIdModel(object):
    def __init__(self, value: str):
        if not re.match("^[a-fA-F0-9]{64}$", value):
            raise ValueError("Invalid Model")
        self.id = value


class StorageModel(object):
    def __init__(self):
        self.paths: List[str] = []
        self.ids: Dict[str, int] = {}

    def add_tracks(self, paths: List[PathModel]):
        for each in paths:
            self.paths.append(each.filename)
            self.ids[self.to_id(each.filename).id] = len(self.paths) - 1

    @staticmethod
    def to_id(raw: str) -> TrackIdModel:
        return TrackIdModel(sha256(raw.encode()).hexdigest())

    @staticmethod
    def from_id(raw: TrackIdModel) -> str:
        return raw.id

    def paginate_tracks(self, model: PaginateModel) -> Dict[TrackIdModel, str]:
        if model.index + model.count >= len(self.paths):
            return dict()
        paths: List[str] = self.paths[model.index: model.index + model.count]
        return dict(zip(map(self.to_id, paths), paths))

    def get_path(self, value: TrackIdModel) -> Optional[str]:
        try:
            idx = self.ids[value.id]
        except KeyError:
            return None
        try:
            return self.paths[idx]
        except IndexError:
            return None
