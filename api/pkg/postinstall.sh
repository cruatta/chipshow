#!/bin/sh

cleanup() {
    printf "\033[32m Cleanup of install\033[0m\n"
}

cleanInstall() {
    printf "\033[32m Post Install of an clean install\033[0m\n"

    python3 -m pip install /usr/share/chipshow/*.whl

    printf "\033[32m Reload the service unit from disk\033[0m\n"
    systemctl daemon-reload ||:
    printf "\033[32m Unmask the service\033[0m\n"
    systemctl unmask chipshow-api
}

upgrade() {
    printf "\033[32m Post Install of an upgrade\033[0m\n"

    python3 -m pip install --upgrade /usr/share/chipshow/*.whl
}

# Step 2, check if this is a clean install or an upgrade
action="$1"
if  [ "$1" = "configure" ] && [ -z "$2" ]; then
  # Alpine linux does not pass args, and deb passes $1=configure
  action="install"
elif [ "$1" = "configure" ] && [ -n "$2" ]; then
    # deb passes $1=configure $2=<current version>
    action="upgrade"
fi

case "$action" in
  "1" | "install")
    cleanInstall
    ;;
  "2" | "upgrade")
    printf "\033[32m Post Install of an upgrade\033[0m\n"
    upgrade
    ;;
  *)
    # $1 == version being installed
    printf "\033[32m Alpine\033[0m"
    cleanInstall
    ;;
esac

# Step 4, clean up unused files, yes you get a warning when you remove the package, but that is ok.
cleanup