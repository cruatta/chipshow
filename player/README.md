# Chipshow
Tracked music AKA "chiptunes" are beautiful in both their sound and their form. These songs contain note, instrument, and effect data all in a single package organized into tracks, channels, and frames. This demo uses an LED Matrix to bring the contents of these songs to life, synchronizing music with the module data to display the notes of these old school tunes as they play.     

![](chipshow.webm)

# Requirements
## Hardware
- [A Raspberry Pi 3 Model B](https://www.adafruit.com/product/3055) or B+ if you like WiFi
- [32x32 RGB LED Matrix Panel - 4mm Pitch](https://www.adafruit.com/product/607) or another 32x32 RGB LED Matrix Panel from Adafruit
- [Adafruit RGB Matrix HAT + RTC for Raspberry Pi - Mini Kit](https://www.adafruit.com/product/2345)
- [5V 4A (4000mA) switching power supply](https://www.adafruit.com/product/1466)
- [A USB Speaker](https://www.amazon.com/HONKYOB-Speaker-Computer-Multimedia-Notebook/dp/B075M7FHM1/)
- Something to put the OS on

## Software
OS: 32bit Raspbian Lite

Libraries:
- libasound2-dev 1.2.4-1.1+rpt1 (for ALSA) (Other versions probably work but this is what I'm using)
- libxmp-dev 4.5.0 (for libxmp)
- liblist (for a pretty good linked list)
- librgbmatrix (_the_ driver for the RGB Matrix)
- Misc GNU/Linux libraries which should come with the OS


