/* A front-end for libxmp that displays pattern data */
/* This file is in public domain */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xmp.h>
#include <list.h>
#include <signal.h>
#include <unistd.h>
#include <libgen.h>

#include "led-matrix-c.h"
#include "sound.h"

static int panel_height = 32;
static int panel_width = 32;

const char *font_file = "/usr/local/share/bdf/6x9.bdf";

struct rgb_t {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

static struct rgb_t notes_colors[] = {
	//"C ", "C#", "D ", "D#", "E ", "F ", "F#", "G ", "G#", "A ", "A#", "B "
	//white, red, lime, blue, yellow, magenta, silver, maroon, green, purple. green, navy
	{255,255,255}, {255,0,0}, {0,255,0}, {0,0,255}, {255,255,0}, {255,0,255}, {192,192,192}, {128,0,0}, {0,128,0}, {128,0,128}, {0,128,128}, {0,0,128}
};

struct RGBLedMatrix *matrix;

static struct LedCanvas* display_mod_info(char *filename, struct xmp_module_info *mi, struct RGBLedMatrix *matrix, struct LedCanvas *offscreen_canvas) {
	char *text = strlen(mi->mod->name) > 0 ? mi->mod->name : filename;
	printf("%s (%s) chn:(%d)\n", text, mi->mod->type, mi->mod->chn);

	int length = 0;
	int loops = 1;
	int letter_spacing = 0;
	int speed = 1;

	struct LedFont *font = load_font(font_file); 
	
	int x = panel_width;
	/* center text vertically */
	int y = (baseline_font(font) + panel_height) / 2;

	struct LedCanvas *canvas = offscreen_canvas;

	while (loops != 0) {
		led_canvas_clear(canvas);
		// length = holds how many pixels our text takes up
	  	length = draw_text(canvas, font, x, y, 255, 255, 255, text, letter_spacing);

		if (speed > 0 && --x + length < 0) {
			x = 0;
			if (loops > 0) --loops;
	    	}

    		// Swap the offscreen_canvas with canvas on vsync, avoids flickering
    		canvas = led_matrix_swap_on_vsync(matrix, canvas);
    		usleep(50000);
  	}

	delete_font(font);
	return canvas;
}

static struct LedCanvas* display_tracker(list_t *patterns, struct RGBLedMatrix *matrix, struct LedCanvas *offscreen_canvas)
{
	uint8_t r = 255;
	uint8_t g = 255;
	uint8_t b = 255;

	list_node_t *node;
	list_iterator_t *it = list_iterator_new(patterns, LIST_HEAD);
	for(int y = 0; y < panel_height; y++) {
		for(int x = panel_width - 1; x >= 0; x--) {
			node = list_iterator_next(it);
			if(node != NULL) {
				r = (*(struct rgb_t*)node->val).r;
				g = (*(struct rgb_t*)node->val).g;
				b = (*(struct rgb_t*)node->val).b;
				//printf("%02x,%02x,%02x", r,g,b);
        			led_canvas_set_pixel(offscreen_canvas, x, y, r, g, b);
			} else {
				// black
				led_canvas_set_pixel(offscreen_canvas, x, y, 0, 0, 0);
			}
		}
	}
	list_iterator_destroy(it);

    	struct LedCanvas *swapped_canvas = led_matrix_swap_on_vsync(matrix, offscreen_canvas);
	return swapped_canvas;
}

static struct rgb_t* rgb_new(uint8_t r, uint8_t g, uint8_t b) {
	struct rgb_t* rgb = malloc(sizeof (struct rgb_t));
	rgb->r = r;
	rgb->g = g;
	rgb->b = b;
	return rgb;
}

static void update_patterns(struct xmp_module_info *mi, struct xmp_frame_info *fi, list_t *patterns)
{
	for (int i = 0; i < panel_width; i++) {
		if (i >= mi->mod->chn) {
			struct rgb_t *black = rgb_new(0,0,0);;
			list_lpush(patterns, list_node_new(black));
		} else {
			int track = mi->mod->xxp[fi->pattern]->index[i];
			struct xmp_event *event = &mi->mod->xxt[track]->event[fi->row];
			if (event->note > 0x80) {
				struct rgb_t *pink = rgb_new(255,170,187);
				list_lpush(patterns, list_node_new(pink));
			} else if (event->note > 0) {
				int note = event->note - 1;
				struct rgb_t color = notes_colors[note % 12];
				list_lpush(patterns, list_node_new(rgb_new(color.r, color.g, color.b)));
			} else {
				struct rgb_t *black = rgb_new(0,0,0);
				list_lpush(patterns, list_node_new(black));
			}
		}
		list_node_t *tail = list_at(patterns, -1);
		if (tail != NULL) {
			// should free the val as well according to docs
			list_remove(patterns, tail);
		}
	}
}

void sig_handler(int signum)
{
	if(matrix != NULL) {
		led_matrix_delete(matrix);
	}
	//sound_deinit();
}

int main(int argc, char **argv)
{
	signal(SIGINT,sig_handler); // Register cleanup handler

	xmp_context ctx;
	struct xmp_module_info mi;
	struct xmp_frame_info fi;
	int row, pos, i;
	list_t *patterns = list_new();
	
	/* initialize sound, which _must_ happen before the matrix otherwise we get an alsa error */
	if (sound_init(44100, 2) < 0) {
		fprintf(stderr, "%s: can't initialize sound\n", argv[0]);
		exit(1);
	}

	/* initialize matrix */
	struct RGBLedMatrixOptions options;

  	memset(&options, 0, sizeof(options));
  	options.rows = panel_width;
  	options.chain_length = 1;
	options.hardware_mapping = "adafruit-hat";
	matrix = led_matrix_create_from_options(&options, &argc, &argv);
	if (matrix == NULL) {
		fprintf(stderr, "Matrix fails to load");
		exit(2);
	}

	struct LedCanvas *offscreen_canvas;
	offscreen_canvas = led_matrix_create_offscreen_canvas(matrix);

	/* initialize patterns */
	for (i = 0; i < panel_width * panel_height; i++) {
		struct rgb_t black = {0,0,0};
		list_lpush(patterns, list_node_new(&black));
	}

	/* create xmp context */
	ctx = xmp_create_context();
	for (i = 1; i < argc; i++) {
		char *filename = basename(argv[i]);
		int res = xmp_load_module(ctx, argv[i]);
		if (res < 0) {
			fprintf(stderr, "%s: error loading %s with error code %d\n", argv[0],
				argv[i], res);
			if(res == -XMP_ERROR_SYSTEM) {
				fprintf(stderr, "errno: %d\n", xmp_syserrno());
			}
			continue;
		}

		if (xmp_start_player(ctx, 44100, 0) == 0) {

			xmp_get_module_info(ctx, &mi);

			/* Show module data */
			offscreen_canvas = display_mod_info(filename, &mi, matrix, offscreen_canvas);

			/* Play module */
			row = pos = -1;
			while (xmp_play_frame(ctx) == 0) {
				xmp_get_frame_info(ctx, &fi);

				if (fi.loop_count > 0)
					break;

				if (fi.pos != pos) {
					pos = fi.pos;
					row = -1;
				}
				if (fi.row != row) {
					update_patterns(&mi, &fi, patterns);
					offscreen_canvas = display_tracker(patterns, matrix, offscreen_canvas);
					row = fi.row;
				}

				sound_play(fi.buffer, fi.buffer_size);
			}
			xmp_end_player(ctx);
		}
		xmp_release_module(ctx);
	}
	led_matrix_delete(matrix);
	xmp_free_context(ctx);
	sound_deinit();
	list_destroy(patterns);

	return 0;
}
