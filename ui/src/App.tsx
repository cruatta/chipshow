import React from "react";
import styled from "styled-components";
import SongList from "./components/SongList";
import { DefaultApiClient } from "./client";
require("./fonts/Topaz_a500_v1.0.ttf");

const Header = styled.header`
  background-color: #333;
  padding: 20px;
  font-family: "Topaz-8", monospace;
  color: white;
  text-align: center;
  font-size: 24px;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    font-size: 20px;
    padding: 15px;
  }
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10px;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const StopButton = styled.button`
  font-size: 16px;
  cursor: pointer;
  background-color: #c0392b;
  color: #fff;
  border-radius: 3px;
  padding: 10px 20px;
  border: none;
  font-family: "Topaz-8", monospace;
  display: block;
  margin: 0 10px;

  @media (max-width: 768px) {
    margin: 5px 0;
  }
`;

const VolumeButton = styled.button`
  font-size: 16px;
  cursor: pointer;
  background-color: #3498db;
  color: #fff;
  border-radius: 3px;
  padding: 10px 20px;
  border: none;
  font-family: "Topaz-8", monospace;
  margin: 0 10px;

  @media (max-width: 768px) {
    margin: 5px 0;
  }
`;

const decreaseVolume = async () => {
  await DefaultApiClient.decreaseVolumeApiVolumeDecreasePut();
};

const increaseVolume = async () => {
  await DefaultApiClient.increaseVolumeApiVolumeIncreasePut();
};

const App: React.FC = () => {
  const playSong = async (trackId: string) => {
    await DefaultApiClient.playTrackApiPlayerTrackIdPut(trackId);
  };

  const stopPlayer = async () => {
    await DefaultApiClient.stopApiPlayerDelete();
  };

  return (
    <div>
      <Header>Chipshow</Header>
      <SongList pageSize={50} onPlaySong={playSong} />
      <ButtonsContainer>
        <VolumeButton onClick={decreaseVolume}>Decrease Volume</VolumeButton>
        <StopButton onClick={stopPlayer}>Stop</StopButton>
        <VolumeButton onClick={increaseVolume}>Increase Volume</VolumeButton>
      </ButtonsContainer>
    </div>
  );
};

export default App;
