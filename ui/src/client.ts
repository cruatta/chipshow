import { Configuration, DefaultApiFactory } from "./api";

const baseURL = `${window.location.protocol}//${window.location.host}`;
export const DefaultApiClient = DefaultApiFactory(
  new Configuration({ basePath: baseURL })
);
