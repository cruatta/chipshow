// src/components/Song.tsx
import React from "react";
import styled from "styled-components";
import { TrackPath } from "../api";

const SongContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  font-size: 14px;
  font-family: "Topaz-8", monospace;
  background-color: #eee;
  border-bottom: 1px solid #ccc;
`;

const Title = styled.div`
  font-size: 18px;
`;

const PlayButton = styled.button`
  font-size: 14px;
  cursor: pointer;
  background-color: #333;
  color: #fff;
  border-radius: 3px;
  padding: 5px 10px;
  border: none;
  font-family: "Topaz-8", monospace;
`;

interface SongProps {
  song: TrackPath;
  onPlay: (song: TrackPath) => void;
}

const Song: React.FC<SongProps> = ({ song, onPlay }) => {
  return (
    <SongContainer>
      <Title>{song.path}</Title>
      <PlayButton onClick={() => onPlay(song)}>Play</PlayButton>
    </SongContainer>
  );
};

export default Song;
