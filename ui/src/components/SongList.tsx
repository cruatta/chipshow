// src/components/SongList.tsx
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { TrackPath } from "../api";
import Song from "./Song";
import { DefaultApiClient } from "../client";

const Container = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: 0 10px;

  @media (max-width: 768px) {
    max-width: 100%;
  }
`;

const Pagination = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

const Button = styled.button`
  font-size: 16px;
  cursor: pointer;
  background-color: #9b59b6; /* Purple */
  color: white;
  border-radius: 3px;
  padding: 10px 20px;
  border: none;
  font-family: "Topaz-8", monospace;

  &:hover {
    background-color: #8e44ad; /* Darker purple */
  }

  &:disabled {
    background-color: #bdc3c7; /* Grey */
    cursor: not-allowed;
  }

  @media (max-width: 768px) {
    margin-bottom: 10px;
  }
`;

const PageNumber = styled.div`
  font-size: 16px;
  font-family: "Topaz-8", monospace;
  text-align: center;
  color: white;

  @media (max-width: 768px) {
    margin-bottom: 10px;
  }
`;

const EmptyMessage = styled.div`
  text-align: center;
  font-size: 24px;
  font-family: "Topaz-8", monospace;
  color: #333;
  padding: 20px;
`;

interface SongListProps {
  pageSize: number;
  onPlaySong: (trackId: string) => void;
}

function isArrayTrackPath(unknown: unknown): unknown is Array<TrackPath> {
  return (
    Array.isArray(unknown) &&
    unknown.every(
      (each) =>
        typeof each.track_id === "string" && typeof each.path === "string"
    )
  );
}

const SongList: React.FC<SongListProps> = ({ pageSize, onPlaySong }) => {
  const [songs, setSongs] = useState([] as TrackPath[]);
  const [currentPage, setCurrentPage] = useState(0);
  const [hasMore, setHasMore] = useState(true);

  useEffect(() => {
    loadSongs(currentPage);
  }, [currentPage]);

  const loadSongs = async (page: number) => {
    const response = await DefaultApiClient.listTracksApiTrackCountGet(
      pageSize,
      pageSize * page
    );
    if (isArrayTrackPath(response.data)) {
      setSongs(response.data);

      if (response.data.length === 0) {
        setHasMore(false);
      } else {
        setHasMore(true);
      }
    } else {
      setSongs([]);
      setHasMore(false);
    }
  };

  const playSong = (trackPath: TrackPath) => {
    onPlaySong(trackPath.track_id);
  };

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  const handlePreviousPage = () => {
    setCurrentPage(currentPage - 1);
  };

  return (
    <Container>
      {songs.length === 0 ? (
        <EmptyMessage>No mas</EmptyMessage>
      ) : (
        songs.map((song) => (
          <Song key={song.path} song={song} onPlay={playSong} />
        ))
      )}
      <Pagination>
        <Button onClick={handlePreviousPage} disabled={currentPage === 0}>
          Back
        </Button>
        <PageNumber>Page {currentPage + 1}</PageNumber>
        <Button onClick={handleNextPage} disabled={!hasMore}>
          Next
        </Button>
      </Pagination>
    </Container>
  );
};

export default SongList;
